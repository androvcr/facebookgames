/**
	Create by Andres Rojas @ StartX Consulting 2014
*/
$(document).ready(function(){
	$.ajax({							//Loading JSON
	  	url: "data/mockup.json",
		  context: document.body
		}).done(function(data) {
	  		createContent(data);
	});
});

function createContent(data){			//Once loaded we need to add all the sections that we got from JSON
	var content = $(".content-wrapper");
	var buttons = $('li');
	for(var i=0;i<data.length;i++){
		content.append(getArticulo(data,buttons,i));
	}
	moveContent($('li').first());		//Activate the first section
}

function getArticulo(fullData,buttons,index){	//Create a new article tag for data
	var data = fullData[index];				//getting data
	var button = $(buttons[index]);			//getting related button
	var art = $("<article>");
	var title = $("<title>");
	var img = $("<div>",{class:'content-header-img',id:data.name+'-img'});
	var header = $("<div>");		//Header
	title.append(img);
	title.append(header);
	var h3 = $("<h3>",{html:data.title,class:'fs-30 fw-semibold fc-cyan1'});
	var titleSpan = $("<span>",{html:data.subtitle,class:'fs-18 flh-20 fc-gray3 fw-light'});
	var content = $("<div>",{class:'content-data helvetica fs-13 fc-gray2',html:data.content});		//Content
	art.append(title);
	header.append(h3);
	header.append(titleSpan);
	art.append(content);
	var footer = $('<div>',{class:'helvetica fs-13 fc-gray2',html:'<p><a class="link fc-cyan2">Kathleen Baker</a>, <a class="link fc-cyan2">Ryan Larsen</a> and 4 Other Friends are doing this step.</p>'});
	art.append(footer);	//Footer
	button.data('reference',index);		//adding section reference to this button
	button.click(function(){
		moveContent($(this));
	});
	return art;
}
// Used when user click a button
function moveContent(button){	
	$('.selected').removeClass('selected');	//Remove the current selected item
	button.addClass('selected');
	var index = button.data('reference');	//getting section reference
	TweenMax.to($('.content-wrapper'),1,{marginTop:-(index*270),ease:Back.easeOut});	//animate
}