package cubicleGame
{	
	import cubicleGame.controller.GalleryViewController;
	import cubicleGame.controller.MenuViewController;
	import cubicleGame.controller.PopUpController;
	import cubicleGame.controller.StageViewController;
	import cubicleGame.model.GameEvents;
	import cubicleGame.model.MainModel;
	import cubicleGame.view.ImageComponent;
	
	import fl.data.DataProvider;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	[SWF(width='800',height='650',backgroundColor='#ffffff',frameRate='25')]
	
	public class CubicleGame extends Sprite
	{
		private var menuController:MenuViewController;
		private var stageController:StageViewController;
		private var popUpController:PopUpController;
		private var mainModel:MainModel;
		private var galleryController:GalleryViewController;
		
		public function CubicleGame(){
			mainModel = new MainModel();
			mainModel.addEventListener(GameEvents.MAIN_MODEL_READY,onMainModelReadyEventHandler);
			mainModel.addEventListener(GameEvents.IMAGE_UPLOADED,onImageUploadedEventHandler);
			mainModel.init();
		}
		
		private function onMainModelReadyEventHandler(e:GameEvents):void{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onXMLLoadedEventHandler);
			var requestXML:URLRequest = new URLRequest("xml/preset.xml");
			loader.load(requestXML);
		}
		
		private function onAddNewMovieClipEventHandler(e:GameEvents):void{
			stageController.addUploadedImage(e.result as Loader);
		}
		
		private function onSelectImageEventHandler(e:GameEvents):void{
			menuController.setImage(e.result as ImageComponent);
		}
		
		private function onUnselectImageEventHandler(e:GameEvents):void{
			stageController.unselectImage();
		}
		
		private function onRemoveImageEventHandler(e:GameEvents):void{
			stageController.removeCurrentImage();
		}
		
		private function onXMLLoadedEventHandler(e:Event):void{
			var file:URLLoader = e.target as URLLoader;		//getting the target
			var mainXMLData:XML = new XML(file.data);			//Setting the data
			
			var toolbar:ToolBarView = new ToolBarView();
			var gameview:GameView = new GameView();
			var popUpView:PopUpView = new PopUpView();
			
			menuController = new MenuViewController(toolbar);
			menuController.addEventListener(GameEvents.ADD_NEW_MOVIECLIP,onAddNewMovieClipEventHandler);
			menuController.addEventListener(GameEvents.SHOW_GALLERY_VIEW,onShowGalleryViewEventHandler);
			menuController.addEventListener(GameEvents.UNSELECT_IMAGE,onUnselectImageEventHandler);
			menuController.addEventListener(GameEvents.REMOVE_IMAGE,onRemoveImageEventHandler);
			menuController.addEventListener(GameEvents.SHOW_CREDITS,onShowCreditsEventHandler);
			menuController.initHeader(mainModel.userPhoto);
			menuController.addEventListener(GameEvents.MOVE_IMAGE_DOWN,onMoveImageDownEventHandler);
			menuController.addEventListener(GameEvents.MOVE_IMAGE_UP,onMoveImageUpEventHandler);
			menuController.addEventListener(GameEvents.SHARE_TO_FACEBOOK,onShareToFacebookEventHandler);
			stageController = new StageViewController(gameview);
			stageController.presetImagesDataProvider(mainXMLData);
			stageController.addEventListener(GameEvents.SELECT_IMAGE,onSelectImageEventHandler);
			stageController.addEventListener(GameEvents.STAGE_CHANGED,onStageChangedEventHandler);
			popUpController = new PopUpController(popUpView);
			popUpController.addEventListener(GameEvents.SHARE_TO_FACEBOOK,onShareConfirmEventHandler);
			
			var galleryView:GalleryView = new GalleryView();
			galleryController = new GalleryViewController(galleryView);
			galleryController.addEventListener(GameEvents.ADD_NEW_PRESET,onAddNewPresetImageEventHandler);
			galleryController.presetImagesDataProvider(mainXMLData);
			
			addChild(gameview);
			addChild(toolbar);
			addChild(galleryView);
			addChild(popUpView);
			
			changeCurrentStep();
			popUpController.showPopUpComponent(PopUpController.WELCOME_VIEW);
		}
		
		private function onShowGalleryViewEventHandler(e:GameEvents):void{
			galleryController.showView();
		}
		private function onAddNewPresetImageEventHandler(e:GameEvents):void{
			galleryController.hideView();
			stageController.addXMLImage(e.result as String);
		}
		
		private function changeCurrentStep():void{
			menuController.currentStep = mainModel.currentStep;
			stageController.currentStep = mainModel.currentStep;
		}
		private function onStageChangedEventHandler(e:GameEvents):void{
			menuController.setImagesDataProvider(e.result as Array);
		}
		private function onMoveImageDownEventHandler(e:GameEvents):void{			
			stageController.moveImageDown();
		}
		private function onMoveImageUpEventHandler(e:GameEvents):void{
			stageController.moveImageUp();
		}
		private function onShowCreditsEventHandler(e:GameEvents):void{
			popUpController.showPopUpComponent(PopUpController.HELP_VIEW);
		}
		private function onShareToFacebookEventHandler(e:GameEvents):void{
			popUpController.showPopUpComponent(PopUpController.SHARE_VIEW);
		}
		private function onShareConfirmEventHandler(e:GameEvents):void{
			popUpController.showPopUpComponent(PopUpController.WAIT_VIEW);
			var image:String = stageController.getScreenShot();
			mainModel.uploadPhotoToBosz(image);
		}
		private function onImageUploadedEventHandler(e:GameEvents):void{
			var finalURL:String = e.result as String;
			popUpController.facebookSharingImageReady(finalURL);
		}
	}
}
