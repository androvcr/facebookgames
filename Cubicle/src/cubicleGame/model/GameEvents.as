package cubicleGame.model
{
	import flash.events.Event;
	
	public final class GameEvents extends Event{
		
		public static const ADD_NEW_MOVIECLIP:String = "addNewMovieClip";
		public static const SHOW_GALLERY_VIEW:String = "showGalleryView";
		public static const SELECT_IMAGE:String = "selectImage";
		public static const UNSELECT_IMAGE:String = "unselectImage";
		public static const REMOVE_IMAGE:String = "removeImage";
		public static const SHOW_CREDITS:String = "showCredits";
		public static const ADD_NEW_PRESET:String = "addNewPreset";
		public static const MAIN_MODEL_READY:String = "mainModelReady";
		public static const STAGE_CHANGED:String = 'stageChanged';
		public static const MOVE_IMAGE_UP:String = 'moveImageUp';
		public static const MOVE_IMAGE_DOWN:String = 'moveImageDown';
		public static const SHARE_TO_FACEBOOK:String = 'shareToFacebook';
		public static const IMAGE_UPLOADED:String = 'imageUploaded';
		
		public var result:Object = null;
		
		public function GameEvents(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event{ 
			var newEvent:GameEvents = new GameEvents(type,bubbles,cancelable);
			newEvent.result = result;
			return newEvent; 
		} 
	}
}