package cubicleGame.model
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.system.Security;
	
	public final class MainModel extends EventDispatcher{
		public static const THUMB_IMAGE_NAME:String = "thumbnail.jpg";
		
		public static const MAIN_MODEL_READY:String = "mainModelReady";
		public static const STEP_1:int = 1;
		public static const STEP_2:int = 2;
		public static const STEP_3:int = 3;
		
		private var _ready:int = 1;
		private var _userID:String = "";
		private var _photoURL:String = "";
		private var _name:String = "";
		private var _process:int = STEP_1;
		private var _objectsArray:Array = [];
		
		public function MainModel(){
			Security.allowDomain("*");
		}
		
		public function init():void{
			var userRequest:URLRequest = new URLRequest();
			userRequest.url = "getUserInfo.php";
			userRequest.method = "POST";
			var tempLoader:URLLoader = new URLLoader();
			tempLoader.addEventListener( Event.COMPLETE, onUserDataLoaded );
			tempLoader.load(userRequest);
		}
		
		private function onUserDataLoaded(e:Event):void{
			var temp:String = e.target.data as String;
			var xmlData:XML = new XML(temp);
			_userID = xmlData.id;
			_name = xmlData.name;
			_photoURL = xmlData.photo;
			checkReady();
		}
		
		private function checkReady():void{
			_ready--;
			if(_ready == 0){
				dispatchEvent(new GameEvents(GameEvents.MAIN_MODEL_READY,true,true));
			}
		}
		
		public function get userID():String{
			return _userID;
		}
		public function get userPhoto():String{
			return _photoURL;
		}
		public function get name():String{
			return _name;
		}
		public function get currentStep():int{
			return _process;
		}
		public function set currentStep(value:int):void{
			_process = value;
		}
		public function uploadPhotoToBosz(image:String):void{
			var variables:URLVariables = new URLVariables();
			var userRequest:URLRequest = new URLRequest();
			userRequest.url = "saveImage.php";
			userRequest.method = "POST";
			variables.imageData = image;
			userRequest.data = variables;
			var tempLoader:URLLoader = new URLLoader();
			tempLoader.addEventListener( Event.COMPLETE, uploadPhotoToBoszCompletedEventHandler );
			tempLoader.load(userRequest);
		}
		private function uploadPhotoToBoszCompletedEventHandler(e:Event):void{
			var finalURL:String = e.target.data as String;
			var gameevent:GameEvents = new GameEvents(GameEvents.IMAGE_UPLOADED);
			gameevent.result = finalURL;
			dispatchEvent(gameevent);
		}
	}
}