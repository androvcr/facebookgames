package cubicleGame.view
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	
	import ws.outset.display.DistortImageWrapper;
	
	public final class ImageLayer extends Sprite{
		
		public static const READY:String = "ready";

		private var value:Loader;
		private var imageMC:MovieClip;
		private var _distortClip:DistortImageWrapper;
		
		public function ImageLayer(__value:Loader){
			super();
			value = __value;
			//Init Component
			addEventListener(Event.ENTER_FRAME,onEnterFrameEventHandler);
			imageMC = new MovieClip;
			imageMC.addChild(value);
		}
		private function onEnterFrameEventHandler(e:Event):void{
			if(imageMC.width > 0){
				removeEventListener(Event.ENTER_FRAME,onEnterFrameEventHandler);
				_distortClip = new DistortImageWrapper(imageMC,5,5);
				addChild(_distortClip);
				dispatchEvent(new Event(READY));
			}
		}
		public function changeColor(value:uint):void{
			var ct:ColorTransform = new ColorTransform();
			ct.color = value;
			transform.colorTransform = ct;
		}
		public function reset():void{
			removeChild(_distortClip);
			_distortClip = new DistortImageWrapper(imageMC,5,5);
			addChild(_distortClip);
		}
		public function set tl(value:Point):void{
			_distortClip.setTL(value.x,value.y);
		}
		public function set tr(value:Point):void{
			_distortClip.setTR(value.x,value.y);
		}
		public function set bl(value:Point):void{
			_distortClip.setBL(value.x,value.y);
		}
		public function set br(value:Point):void{
			_distortClip.setBR(value.x,value.y);
		}
	}
}