package cubicleGame.view{
	import cubicleGame.model.GameEvents;
	import cubicleGame.utils.ButtonCreator;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	
	public final class ImageComponent extends Sprite{
		public static const UPLOADED_IMAGE:String = "uploadedImage";
		public static const PRESET_IMAGE:String = "xmlImage";
		public static const STAGE_IMAGE:String = "stageImage";
		public var icon:String = "";
		private var centerMovie:CenterView;
		private var type:String;
		private var _movible:Boolean = false;
		private var _w:Number = 0;
		private var _h:Number = 0;
		
		public function get movible():Boolean{
			return _movible;
		}
		private var _colorable:int = -1;
		public function get colorable():Boolean{
			return _colorable != -1;
		}
		private var _transformable:Boolean = false;
		public function get transformable():Boolean{
			return _transformable;
		}
		private var _eliminable:Boolean = true;
		public function get eliminable():Boolean{
			return _eliminable;
		}
		private var _rotable:Boolean = false;
		public function get rotable():Boolean{
			return _rotable;
		}
		private var _scalable:Boolean = false;
		public function get scalable():Boolean{
			return _scalable;
		}
		private var layersArray:Array;
		private var _readyCount:int = 0;
		
		private var _tl:Point = null;
		public function get tl():Point{
			return _tl;
		}
		public function set tl(value:Point):void{
			_tl = value;
			for each(var img:ImageLayer in layersArray){
				img.tl = value;
			}
		}
		private var _tr:Point = null;
		public function get tr():Point{
			return _tr;
		}
		public function set tr(value:Point):void{
			_tr = value;
			for each(var img:ImageLayer in layersArray){
				img.tr = value;
			}
		}
		private var _bl:Point = null;
		public function get bl():Point{
			return _bl;
		}
		public function set bl(value:Point):void{
			_bl = value;
			for each(var img:ImageLayer in layersArray){
				img.bl = value;
			}
		}
		private var _br:Point = null;
		public function get br():Point{
			return _br;
		}
		public function set br(value:Point):void{
			_br = value;
			for each(var img:ImageLayer in layersArray){
				img.br = value;
			}
		}
		private var _originalColor:uint = 0;
		private var _currentColor:uint = 0;
		public function get currentColor():uint{
			return _currentColor;
		}
		private var _imageName:String = "";
		public function get label():String{
			return _imageName;
		}
		//------------------------------------------------------------------------------------
		private var _selected:Boolean = false;
		public function set selected(value:Boolean):void{
			_selected = value;
			if(value){
				if(_readyCount == 0){
					ButtonCreator.setGlowEffect(this,0xf0b310);
					centerMovie.visible = true;
				}
			}else{
				ButtonCreator.setGlowEffect(this,0);
				centerMovie.visible = false;
			}
		}
		public function get selected():Boolean{
			return _selected;
		}
		//------------------------------------------------------------------------------------
		public function ImageComponent(_value:Object, _type:String,count:int){
			layersArray = new Array();
			centerMovie = new CenterView();
			type = _type;
			visible = false;
			switch(type){
				case UPLOADED_IMAGE:
					_readyCount = 1;
					var tempLoader:ImageLayer = new ImageLayer(_value as Loader);
					tempLoader.addEventListener(ImageLayer.READY,onImageReadyEventHandler);
					layersArray.push(tempLoader);
					addChild(tempLoader);
					layersArray.push(tempLoader);
					_colorable = -1;
					_movible = true;
					_transformable = true;
					_rotable = true;
					_scalable = true;
					_imageName = 'uploaded image';
					x = y = -1;
				break;
				
				case STAGE_IMAGE:
					_eliminable = false;
					
				case PRESET_IMAGE:
					_readyCount = parseInt(_value.imageCount.toString());
					_colorable = parseInt(_value.colorable) - 1;
					for(var i:int=0;i<_readyCount;i++){
						var loader:Loader = new Loader();
						var name:String = 'images/' + (_value.name.toString()) + '/' + (i+1) + ".png";
						loader.load(new URLRequest(name));
						var newImage:ImageLayer = new ImageLayer(loader);
						newImage.addEventListener(ImageLayer.READY,onImageReadyEventHandler);
						addChild(newImage);
						layersArray.push(newImage);
					}
					_movible = getBooleanFromString(_value.movible);
					_transformable = getBooleanFromString(_value.transformable);
					_rotable = getBooleanFromString(_value.rotable);
					_scalable = getBooleanFromString(_value.scalable);
					_imageName = _value.name.toString();
					
					_currentColor = Math.random() * 0xFFFFFF;
					_originalColor = _currentColor;
					changeColor(_currentColor);
					
					if(!movible){
						x = _value.x;
						y = _value.y;
					}else{
						x = y = -1;
					}
				break;
			}
			_imageName += '' + count;
			addEventListener(MouseEvent.ROLL_OVER,onRollOverEventHandler);
			addEventListener(MouseEvent.ROLL_OUT,onRollOutEventHandler);
		}
		
		private function onImageReadyEventHandler(e:Event):void{
			_readyCount--;
			if(_readyCount == 0){
				visible = true;
				selected = _selected;
				centerMovie.visible = false;
				addChild(centerMovie);
				var movie:ImageLayer = layersArray[0] as ImageLayer;
				_w = movie.width;
				_h = movie.height;
				_tl = new Point(-_w/2,-_h/2);
				_tr = new Point(_w/2,-_h/2);
				_bl = new Point(-_w/2,_h/2);
				_br = new Point(_w/2,_h/2);
				if(x == -1 && y == -1){
					x = ((800 - _w)/2) + (_w/2);
					y = ((650 - _h)/2) + (_h/2);
				}
			}
		}
		private function onRollOverEventHandler(e:MouseEvent):void{
			if(!_selected){
				ButtonCreator.setGlowEffect(this,0x00BAFF);
			}
		}
		private function onRollOutEventHandler(e:MouseEvent):void{
			if(!_selected){
				ButtonCreator.setGlowEffect(this,0);	
			}
		}
		//------------------------------------------------------------------------------------------------------------------------------
		public function moveImage(value:Boolean):void{
			if(value){
				addEventListener(MouseEvent.MOUSE_DOWN,onMouseDownEventHandler);
				addEventListener(MouseEvent.MOUSE_UP,onMouseUpEventHandler);
			}else{
				removeEventListener(MouseEvent.MOUSE_DOWN,onMouseDownEventHandler);
				removeEventListener(MouseEvent.MOUSE_UP,onMouseUpEventHandler);
				stopDrag();
			}
		}
		private function onMouseDownEventHandler(e:MouseEvent):void{
			startDrag();
		}
		private function onMouseUpEventHandler(e:MouseEvent):void{
			stopDrag();
		}
		private function getBooleanFromString(value:String):Boolean{
			return (value == "true");
		}
		//------------------------------------------------------------------------------------------------------------------------------
		public function changeColor(value:int):void{
			if(colorable){
				_currentColor = value;
				var layer:ImageLayer = layersArray[_colorable];
				layer.changeColor(value);
			}
		}
		public function rotateImage(value:Number):void{
			rotation = value;
		}
		public function forceClickImage():void{
			dispatchEvent(new MouseEvent(MouseEvent.CLICK,true));
		}
		public function reset():void{
			changeColor(_originalColor);
			for each(var img:ImageLayer in layersArray){
				img.reset();
			}
			rotation = 0;
			scaleX = 1;
			scaleY = 1;
			x = 200;
			y = 200;
			_tl = new Point(-_w/2,-_h/2);
			_tr = new Point(_w/2,-_h/2);
			_bl = new Point(-_w/2,_h/2);
			_br = new Point(_w/2,_h/2);
		}
	}
}