package cubicleGame.controller
{
	import cubicleGame.model.GameEvents;
	import cubicleGame.model.MainModel;
	import cubicleGame.utils.ButtonCreator;
	import cubicleGame.view.ImageComponent;
	
	import fl.data.DataProvider;
	import fl.events.ColorPickerEvent;
	import fl.events.SliderEvent;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLRequest;

	public final class MenuViewController extends EventDispatcher{
		
		private var view:ToolBarView = null;
		private var selectedOption:MovieClip = null;
		private var currentImage:ImageComponent = null;
		private var file:FileReference = null;
		private var _currentStep:int = MainModel.STEP_1;
		
		private static const MOVE_LABEL:String = "Move";
		private static const TRANSFORM_LABEL:String = "Distort";
		private static const COLOR_LABEL:String = "Colorize";
		private static const DELETE_LABEL:String = "Delete";
		private static const RESET_LABEL:String = "Reset";
		private static const BACK_LABEL:String = "Back";
		private static const UPLOAD_LABEL:String = "Upload";
		private static const PRESET_LABEL:String = "Preset Images";
		
		public function MenuViewController(_view:ToolBarView){
			view = _view;
			ButtonCreator.createNormalButtonFunctions(view.uploadButton,onUploadButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.presetButton,onPresetButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.backButton,onNormalButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.deleteButton,onNormalButtonClickedEventHandler);
			ButtonCreator.createToggleButtonFunctions(view.moveButton,onOptionClickedEventHandler);
			ButtonCreator.createToggleButtonFunctions(view.transformButton,onOptionClickedEventHandler);
			ButtonCreator.createColorPickerButtonFunctions(view.colorPicker,onColorButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.creditsButton,onCreditsClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.fullButton,onFullButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.upButton,onUpButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.downButton,onDownButtonClickedEventHandler);
			ButtonCreator.createNormalButtonFunctions(view.shareButton,onShareButtonClickedEventHandler);
			view.rotateSlider.addEventListener(SliderEvent.CHANGE,onRotateSliderChangeEventHandler);
			view.scaleSlider.addEventListener(SliderEvent.CHANGE,onScaleSliderChangeEventHandler);
			//----------------------------------------------------------------------
			view.transformView.visible = false;
			view.transformView.topLeftButton.addEventListener(MouseEvent.MOUSE_DOWN,onTransformViewButtonMouseDownEventHandler);
			view.transformView.topLeftButton.addEventListener(MouseEvent.MOUSE_UP,onTransformViewButtonMouseUpEventHandler);
			view.transformView.topRightButton.addEventListener(MouseEvent.MOUSE_DOWN,onTransformViewButtonMouseDownEventHandler);
			view.transformView.topRightButton.addEventListener(MouseEvent.MOUSE_UP,onTransformViewButtonMouseUpEventHandler);
			view.transformView.bottomLeftButton.addEventListener(MouseEvent.MOUSE_DOWN,onTransformViewButtonMouseDownEventHandler);
			view.transformView.bottomLeftButton.addEventListener(MouseEvent.MOUSE_UP,onTransformViewButtonMouseUpEventHandler);
			view.transformView.bottomRightButton.addEventListener(MouseEvent.MOUSE_DOWN,onTransformViewButtonMouseDownEventHandler);
			view.transformView.bottomRightButton.addEventListener(MouseEvent.MOUSE_UP,onTransformViewButtonMouseUpEventHandler);
			//----------------------------------------------------------------------
			view.imageDropDown.addEventListener(Event.CHANGE,onDropDownChangedEventHandler);
			enableComponent(view.moveButton,false);
			enableComponent(view.transformButton,false);
			enableComponent(view.colorPicker,false);
			enableComponent(view.deleteButton,false);
			enableComponent(view.backButton,false);
		}
		public function initHeader(_photo:String):void{
			var photo:PhotoView = view.photoView;
			var loader:Loader = new Loader();
			loader.load(new URLRequest(_photo));
			photo.photoHolder.addChild(loader);
		}
		public function set currentStep(value:int):void{
			_currentStep = value;
			switch(value){
				case MainModel.STEP_1:
					enableComponent(view.uploadButton,true);
					enableComponent(view.presetButton,true);
				break;
				case MainModel.STEP_2:
				break;
			}
			if(currentImage){
				enableComponent(view.moveButton,false);
				enableComponent(view.transformButton,false);
				enableComponent(view.colorPicker,false);
				enableComponent(view.deleteButton,false);
				enableComponent(view.backButton,false);
				dispatchEvent(new GameEvents(GameEvents.UNSELECT_IMAGE,true,true));
			}
		}
		private function enableComponent(e:Object,value:Boolean):void{
			if(value)
				e.alpha = 1;
			else
				e.alpha = 0.5;
			e.mouseChildren = value;
			e.mouseEnabled = value;
			if(e != view.colorPicker)
				e.enabled = value;
		}
		public function setImage(image:ImageComponent):void{
			if(selectedOption){
				checkOption(selectedOption,false);
			}
			currentImage = image;
			enableComponent(view.backButton,true);
			enableComponent(view.deleteButton, currentImage.eliminable);
			enableComponent(view.colorPicker,currentImage.colorable);
			enableComponent(view.transformButton,currentImage.transformable);
			enableComponent(view.moveButton,currentImage.movible);
			enableComponent(view.rotateSlider,currentImage.rotable);
			enableComponent(view.scaleSlider,currentImage.scalable);
			if(currentImage.movible){
				selectedOption = view.moveButton;
				checkOption(selectedOption,true);
			}
			if(currentImage.colorable){
				view.colorPicker.selectedColor = currentImage.currentColor;
			}
			view.rotateSlider.value = currentImage.rotation;
			view.scaleSlider.value = currentImage.scaleX;  
			view.rotateSlider.enabled = true;
			view.rotateSlider.enabled = true;
			view.imageDropDown.selectedItem = image;
			setTransformScale(currentImage.scaleX);
		}
		private function onOptionClickedEventHandler(e:MouseEvent):void{
			var currentOption:MovieClip = e.currentTarget as MovieClip;
			if(currentOption != selectedOption){
				//SoundManager.playClickButtonSound();
			}
			if(selectedOption){
				checkOption(selectedOption, false);
			}
			selectedOption = currentOption;
			checkOption(selectedOption, true);
		}
		
		private function setTransformScale(value:Number):void{
			view.transformView.scaleX = view.transformView.scaleY = value;
			var fixValue:Number = 0;
			if(value < 1){	//thinner
				fixValue = 1-value;
				fixValue = 1+fixValue;
			}else{			//bigger
				fixValue = value-1;
				fixValue = 1-fixValue;
			}
			/*view.transformView.topLeftButton.scaleX = view.transformView.topLeftButton.scaleY = fixValue;
			view.transformView.topRightButton.scaleX = view.transformView.topRightButton.scaleY = fixValue;
			view.transformView.bottomLeftButton.scaleX = view.transformView.bottomLeftButton.scaleY = fixValue;
			view.transformView.bottomRightButton.scaleX = view.transformView.bottomRightButton.scaleY = fixValue;*/
		}
		private function checkOption(option:MovieClip, selected:Boolean):void{
			switch(option){
				case view.moveButton:
					currentImage.moveImage(selected);
					break;
				case view.transformButton:
					view.transformView.visible = selected;
					if(selected){
						view.transformView.x = currentImage.x;
						view.transformView.y = currentImage.y;
						view.transformView.rotation = currentImage.rotation;
						view.transformView.topLeftButton.x = currentImage.tl.x;
						view.transformView.topLeftButton.y = currentImage.tl.y;
						view.transformView.topRightButton.x = currentImage.tr.x;
						view.transformView.topRightButton.y = currentImage.tr.y;
						view.transformView.bottomLeftButton.x = currentImage.bl.x;
						view.transformView.bottomLeftButton.y = currentImage.bl.y;
						view.transformView.bottomRightButton.x = currentImage.br.x;
						view.transformView.bottomRightButton.y = currentImage.br.y;
					}else{
						view.transformView.topLeftButton.stopDrag();
						view.transformView.topRightButton.stopDrag();
						view.transformView.bottomLeftButton.stopDrag();
						view.transformView.bottomRightButton.stopDrag();
					}
					break;
			}
			option.selected = selected;
			option.gotoAndStop("_"+selected);
		}
		private function onNormalButtonClickedEventHandler(e:MouseEvent):void{
			var option:MovieClip = e.currentTarget as MovieClip;
			if(selectedOption)
				checkOption(selectedOption, false);
			selectedOption = null;
			switch(option){
				case view.deleteButton:
					currentImage = null;
					dispatchEvent(new GameEvents(GameEvents.REMOVE_IMAGE,true,true));
					break;
				case view.backButton:
					currentImage.reset();
					checkOption(view.moveButton, true);
					break;
			}
		}
		private function onScaleChangeEventHandler(e:Event):void{
		}
		private function onColorButtonClickedEventHandler(e:ColorPickerEvent):void{
			currentImage.changeColor(view.colorPicker.selectedColor);
		}
		private function onUploadButtonClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();
			file = new FileReference();
			file.addEventListener(Event.SELECT, onFileSelectedEventHandler);
			var imgTypeFilter:FileFilter = new FileFilter("Image Files","*.jpeg; *.jpg;*.gif;*.png");
			file.browse([imgTypeFilter]);
		}
		private function onFileSelectedEventHandler(e:Event):void{
			file.addEventListener(Event.COMPLETE, onFileLoadedEventHandler);
			file.load();
		}
		private function onFileLoadedEventHandler(e:Event):void{
			trace("AddMenuController.onFileLoadedEventHandler");
			var movie:Loader = new Loader();
			movie.loadBytes(file.data);
			var newEvent:GameEvents = new GameEvents(GameEvents.ADD_NEW_MOVIECLIP,true,true);
			newEvent.result = movie;
			dispatchEvent(newEvent);
		}
		private function onPresetButtonClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();
			var newEvent:GameEvents = new GameEvents(GameEvents.SHOW_GALLERY_VIEW,true,true);
			dispatchEvent(newEvent);
		}
		private function onRotateSliderChangeEventHandler(e:SliderEvent):void{
			currentImage.rotateImage(view.rotateSlider.value);
			view.transformView.rotation = view.rotateSlider.value;
		}
		private function onScaleSliderChangeEventHandler(e:SliderEvent):void{
			currentImage.scaleX = currentImage.scaleY = view.scaleSlider.value;
			setTransformScale(view.scaleSlider.value);
		}
		private function onTransformViewButtonMouseDownEventHandler(e:MouseEvent):void{
			(e.currentTarget as MovieClip).startDrag();
		}
		private function onTransformViewButtonMouseUpEventHandler(e:MouseEvent):void{
			var target:MovieClip = e.currentTarget as MovieClip;
			target.stopDrag();
			var newPoint:Point = new Point(target.x,target.y);
			switch(target){
				case view.transformView.topLeftButton:
					currentImage.tl = newPoint;
				break;
				case view.transformView.topRightButton:
					currentImage.tr = newPoint;
					break;
				case view.transformView.bottomLeftButton:
					currentImage.bl = newPoint;
					break;
				case view.transformView.bottomRightButton:
					currentImage.br = newPoint;
					break;
			}
		}
		private function onHowToClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();
		}
		private function onPrizesClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();
		}
		private function onInviteClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();	
		}
		private function onCreditsClickedEventHandler(e:MouseEvent):void{
			//SoundManager.playClickButtonSound();
			var event:GameEvents = new GameEvents(GameEvents.SHOW_CREDITS,true,true);
			dispatchEvent(event);
		}
		private function onFullButtonClickedEventHandler(e:MouseEvent):void{
			switch(view.stage.displayState){
				case StageDisplayState.NORMAL:
					view.stage.displayState = StageDisplayState.FULL_SCREEN;
					break;
				case StageDisplayState.FULL_SCREEN:
					view.stage.displayState = StageDisplayState.NORMAL;
					break;
			}
		}
		public function setImagesDataProvider(value:Array):void{
			view.imageDropDown.removeAll();
			for(var i:int=0;i<value.length;i++){
				var img:ImageComponent = value[i] as ImageComponent;
				view.imageDropDown.addItem(img);
			}
			if(value.length>0){
				if(currentImage == null){
					var firstImage:ImageComponent = value[0] as ImageComponent;
					firstImage.forceClickImage();	
				}else{
					view.imageDropDown.selectedItem = currentImage;
				}
			}else{
				enableComponent(view.moveButton,false);
				enableComponent(view.transformButton,false);
				enableComponent(view.colorPicker,false);
				enableComponent(view.deleteButton,false);
				enableComponent(view.backButton,false);
				view.deleteButton.gotoAndPlay('_up');
				view.rotateSlider.enabled = false;
				view.scaleSlider.enabled = false;
			}
		}
		private function onDropDownChangedEventHandler(e:Event):void{
			var img:ImageComponent = view.imageDropDown.selectedItem as ImageComponent;
			img.forceClickImage();
		}
		private function onUpButtonClickedEventHandler(e:MouseEvent):void{
			var moveEvent:GameEvents = new GameEvents(GameEvents.MOVE_IMAGE_UP,true,true);
			dispatchEvent(moveEvent);
		}
		private function onDownButtonClickedEventHandler(e:MouseEvent):void{
			var moveEvent:GameEvents = new GameEvents(GameEvents.MOVE_IMAGE_DOWN,true,true);
			dispatchEvent(moveEvent);
		}
		private function onShareButtonClickedEventHandler(e:MouseEvent):void{
			var moveEvent:GameEvents = new GameEvents(GameEvents.SHARE_TO_FACEBOOK,true,true);
			dispatchEvent(moveEvent);
		}
	}
}