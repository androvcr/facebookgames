package cubicleGame.controller
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import cubicleGame.model.GameEvents;
	import cubicleGame.model.MainModel;
	import cubicleGame.utils.SoundManager;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public final class PopUpController extends EventDispatcher{
		private var view:PopUpView;
		private var currentContent:MovieClip;
		private var galleryView:GalleryView;
		private var lastImageUploaded:String = '';
		
		public static const WELCOME_VIEW:String = 'welcomeView';
		public static const HELP_VIEW:String = 'helpView';
		public static const WAIT_VIEW:String = 'waitView';
		public static const SHARE_VIEW:String = 'shareView';
		
		public function PopUpController(_view:PopUpView){
			view = _view;
			view.closeButton.addEventListener(MouseEvent.CLICK,onCloseButtonClickEventHandler);
			view.closeButton.addEventListener(MouseEvent.ROLL_OVER,SoundManager.playOverButtonSound);
			view.visible = false;
			view.content.y = 100;
			view.closeButton.y = 100 - (view.closeButton.height/2);
			view.alpha = 0;
		}
		
		private function onCloseButtonClickEventHandler(e:MouseEvent):void{
			SoundManager.playClickButtonSound();
			TweenMax.to(view,0.2,{"alpha":0,"onComplete":hideCompleteEventHandler});
		}
		private function hideCompleteEventHandler():void{
			view.visible = false;
			if(currentContent != galleryView){
				view.content.removeChild(currentContent);
				currentContent = null;
			}else{
				galleryView.visible = false;
			}
		}
		public function showPopUpComponent(component:String):void{
			switch(component){
				case WELCOME_VIEW:
					var welcome:WelcomeView = new WelcomeView();
					welcome.playButton.addEventListener(MouseEvent.CLICK, onCloseButtonClickEventHandler);
					welcome.playButton.buttonMode = true;
					welcome.tutorialButton.addEventListener(MouseEvent.CLICK, showTutorialClickedEventHandler);
					welcome.tutorialButton.buttonMode = true;
					view.closeButton.visible = false;
					currentContent = welcome;
					break;
				case HELP_VIEW:
					var help:HelpView = new HelpView();
					help.howToPlayButton.addEventListener(MouseEvent.CLICK,onHowToPlayButtonClickedEventHandler);
					help.prizesButton.addEventListener(MouseEvent.CLICK,onPrizesButtonClickedEventHandler);
					help.creditsButton.addEventListener(MouseEvent.CLICK,onCreditsButtonClickedEventHandler);
					help.closeButton.addEventListener(MouseEvent.CLICK, onCloseButtonClickEventHandler);
					help.inviteButton.addEventListener(MouseEvent.CLICK,onInviteButtonClickedEventHandler);
					view.closeButton.visible = false;
					currentContent = help;
					break;
				case WAIT_VIEW:
					var wait:WaitView = new WaitView();
					view.closeButton.visible = false;
					currentContent = wait;
					break;
				case SHARE_VIEW:
					var share:ShareView = new ShareView();
					share.okButton.addEventListener(MouseEvent.CLICK,onShareAcceptButtonClickedEventHandler);
					share.cancelButton.addEventListener(MouseEvent.CLICK,onCloseButtonClickEventHandler);
					share.okButton.buttonMode = share.cancelButton.buttonMode = true;
					view.closeButton.visible = false;
					currentContent = share;
					break;
			}
			view.content.addChild(currentContent);
			view.content.x = (800 - currentContent.width) / 2;
			view.closeButton.x = view.content.x + currentContent.width - (view.closeButton.width/2);
			view.closeButton.useHandCursor = true;
			view.visible = true;
			TweenMax.to(view,0.2,{"alpha":1});
		}
		
		private function onHowToPlayButtonClickedEventHandler(e:MouseEvent):void{
			currentContent.gotoAndStop('howTo');
		}
		private function onPrizesButtonClickedEventHandler(e:MouseEvent):void{
			currentContent.gotoAndStop('prizes');
		}
		private function onCreditsButtonClickedEventHandler(e:MouseEvent):void{
			currentContent.gotoAndStop('credits');
		}
		private function showTutorialClickedEventHandler(e:MouseEvent):void{
			hideCompleteEventHandler();
			showPopUpComponent(HELP_VIEW);
		}
		private function onViewPhotoClickedEventHandler(e:MouseEvent):void{
			
		}
		public function facebookSharingImageReady(imageURL:String):void{
			if(imageURL == '-1'){
				trace('Image uploading fail');
				trace(imageURL);
				lastImageUploaded = '';
			}else{
				lastImageUploaded = imageURL;
				var urlR:URLRequest = new URLRequest('http://www.facebook.com/'+lastImageUploaded);
				navigateToURL(urlR,'_top');
			}
		}
		private function onInviteButtonClickedEventHandler(e:MouseEvent):void{
			ExternalInterface.call('inviteFriendsToCubicle');
		}
		private function onShareAcceptButtonClickedEventHandler(e:MouseEvent):void{
			view.content.removeChild(currentContent);
			currentContent = null;
			dispatchEvent(new GameEvents(GameEvents.SHARE_TO_FACEBOOK));
		}
	}
}