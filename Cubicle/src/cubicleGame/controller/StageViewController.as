package cubicleGame.controller
{
	import com.adobe.images.JPGEncoder;
	
	import cubicleGame.model.GameEvents;
	import cubicleGame.model.MainModel;
	import cubicleGame.view.ImageComponent;
	
	import fl.data.DataProvider;
	
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import mx.utils.Base64Encoder;
	
	public final class StageViewController extends Sprite
	{
		private var view:MovieClip = null;
		private var selectedImage:ImageComponent = null;
		private var presetImages:XML = null;
		private var stageScreen:Sprite = null;
		private var objectScreen:Sprite = null;
		private var _imageCounter:int = 0;
		private static const STAGE_TYPE:String = "stage";
		
		public function StageViewController(_view:MovieClip){
			view = _view;
			stageScreen = new Sprite();
			stageScreen.mouseChildren = false;
			stageScreen.mouseEnabled = false;
			
			objectScreen = new Sprite();
			view.addChild(stageScreen);
			view.addChild(objectScreen);
		}
		public function set currentStep(value:int):void{
			switch(value){
				case MainModel.STEP_1:
					break;
			}
		}
		public function presetImagesDataProvider(data:XML):void{
			presetImages = data;
			addStageComponents();
		}
		private function addStageComponents():void{
			var temp:Object = null;
			for each(temp in presetImages.image){
				if(temp.type == STAGE_TYPE){
					var newImage:ImageComponent = new ImageComponent(temp,ImageComponent.STAGE_IMAGE,stageScreen.numChildren);
					addImageToStage(newImage,stageScreen);
				}
			}
		}
		
		//------------------------------------------------------------------------------------------------------------------------------------------------
		public function addUploadedImage(img:Loader):void{
			var newImage:ImageComponent = new ImageComponent(img,ImageComponent.UPLOADED_IMAGE,_imageCounter++);
			addImageToStage(newImage,objectScreen);
		}
		public function addXMLImage(id:String):void{
			var temp:Object = null;
			for each(temp in presetImages.image){
				if(temp.name.toString() == id){
					break;
				}
			}
			var newImage:ImageComponent = new ImageComponent(temp,ImageComponent.PRESET_IMAGE,_imageCounter++);
			addImageToStage(newImage,objectScreen);
		}
		private function addImageToStage(img:ImageComponent, container:Sprite):void{
			img.addEventListener(MouseEvent.CLICK, onMouseClickEventHandler);
			container.addChild(img);
			if(selectedImage){
				selectedImage.selected = false;
				selectedImage = null;
			}
			if(container == objectScreen){
				img.forceClickImage();
				updateImageList();
			}
		}
		private function updateImageList():void{
			var imageArray:Array = [];
			for(var i:int=0;i<objectScreen.numChildren;i++){
				var img:ImageComponent = objectScreen.getChildAt(i) as ImageComponent;
				imageArray.push(img);
			}
			var event:GameEvents = new GameEvents(GameEvents.STAGE_CHANGED);
			imageArray.reverse();
			event.result = imageArray;
			dispatchEvent(event);
		}
		//------------------------------------------------------------------------------------------------------------------------------------------------
		private function onMouseClickEventHandler(e:MouseEvent):void{
			var temp:ImageComponent = e.currentTarget as ImageComponent;
			if(temp != selectedImage){
				if(selectedImage){
					selectedImage.selected = false;
				}
				selectedImage = temp;
				selectedImage.selected = true;
				
				var evt:GameEvents = new GameEvents(GameEvents.SELECT_IMAGE,true,true);
				evt.result = selectedImage;
				dispatchEvent(evt);
			}
		}
		public function unselectImage():void{
			if(selectedImage)
				selectedImage.selected = false;
			selectedImage = null;
		}
		public function removeCurrentImage():void{
			if(selectedImage){
				selectedImage.selected = false;
				objectScreen.removeChild(selectedImage);
				selectedImage = null;
			}
			updateImageList();
		}
		public function moveImageDown():void{
			var currentIndex:int = objectScreen.getChildIndex(selectedImage);
			if(currentIndex>0){
				objectScreen.swapChildrenAt(currentIndex, currentIndex-1);
				updateImageList();
			}
		}
		public function moveImageUp():void{
			var currentIndex:int = objectScreen.getChildIndex(selectedImage);
			if(currentIndex<objectScreen.numChildren-1){
				objectScreen.swapChildrenAt(currentIndex, currentIndex+1);
				updateImageList();
			}
		}
		public function getScreenShot():String{
			unselectImage();
			var bitmapData:BitmapData = new BitmapData(800,650);
			bitmapData.draw(view);
			var jpgEncoder:JPGEncoder = new JPGEncoder(100);
			var base64Encoder:Base64Encoder = new Base64Encoder();
			var imageByteArray:ByteArray = jpgEncoder.encode(bitmapData);
			base64Encoder.encodeBytes(imageByteArray);
			return base64Encoder.toString();
		}
	}
}