package cubicleGame.controller
{
	import cubicleGame.model.GameEvents;
	import cubicleGame.model.MainModel;
	import cubicleGame.utils.ButtonCreator;
	import cubicleGame.view.ImageComponent;
	
	import fl.data.DataProvider;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.net.URLRequest;
	
	public final class GalleryViewController extends EventDispatcher{
		private static const STAGE_TYPE:String = "stage";
		private var view:GalleryView = null;
		private var _currentX:int = 0;
		private var _currentY:int = 0;
		private var _dataProvider:XML = null;
		private var _currentSelectedImage:ImagePreview = null;
		
		private var _currentPage:int = 0;
		private var _pageSize:int = 650;
		private var _pagegroup:Sprite = new Sprite();
		
		public function GalleryViewController(_view:GalleryView){
			view = _view;
			view.visible = false;
			ButtonCreator.createNormalButtonFunctions(view.addButton,onAddToStageButtonClickedEventHandler,null,null);
			ButtonCreator.createNormalButtonFunctions(view.closeButton,onCloseButtonClickedEventHandler,null,null);
			ButtonCreator.createNormalButtonFunctions(view.prevButton,onPrevButtonClickedEventHandler,null,null);
			ButtonCreator.createNormalButtonFunctions(view.nextButton,onNextButtonClickedEventHandler,null,null);
		}
		public function showView():void{
			view.visible = true;
		}
		public function hideView():void{
			view.visible = false;
		}
		
		public function presetImagesDataProvider(data:XML):void{
			_dataProvider = data;
			updateView();
		}
		
		private function updateView():void{
			_currentSelectedImage = null;
			_currentX = _currentY = 0;
			var imagePerPage:int = 18;
			var currentImage:int = 0;
			var currentPage:Sprite = null;
			view.contents.addChild(_pagegroup);
			
			for each(var img:Object in _dataProvider.image){
				if(img.type != STAGE_TYPE){
					if(currentImage%imagePerPage == 0){
						currentPage = new Sprite();
						currentPage.x = _pagegroup.numChildren * _pageSize;
						_pagegroup.addChild(currentPage);
						_currentX = _currentY = 0;
					}
					
					var newImage:ImagePreview = new ImagePreview();
					ButtonCreator.createNormalButtonFunctions(newImage,onImageClickedEventHandler,onImageRollOverEventHandler,onImageRollOutEventHandler);
					newImage.imageName = img.name.toString();
					var loader:Loader = new Loader();
					loader.load(new URLRequest('images/' + img.name + '/' + MainModel.THUMB_IMAGE_NAME));
					newImage.imageHolder.addChild(loader);
					newImage.x = _currentX;
					newImage.y = _currentY;
					currentPage.addChild(newImage);
					
					_currentX += 110;
					if(_currentX > 600){
						_currentX = 0;
						_currentY += 130;
					}
					currentImage++;
				}
			}
		}
		
		private function onImageClickedEventHandler(e:MouseEvent):void{
			var newImage:ImagePreview = e.currentTarget as ImagePreview;
			if(newImage != _currentSelectedImage){
				selectImage(_currentSelectedImage,false);
				selectImage(newImage,true);
				_currentSelectedImage = newImage;	
			}
		}
		private function onImageRollOverEventHandler(e:MouseEvent):void{
			var newImage:ImagePreview = e.currentTarget as ImagePreview;
			if(newImage != _currentSelectedImage){
				var ct:ColorTransform = new ColorTransform();
				ct.color = 0x74D4FC;
				newImage.background.transform.colorTransform = ct;
			}
		}
		private function onImageRollOutEventHandler(e:MouseEvent):void{
			var newImage:ImagePreview = e.currentTarget as ImagePreview;
			if(newImage != _currentSelectedImage){
				var ct:ColorTransform = new ColorTransform();
				ct.color = 0xFFFFFF;
				newImage.background.transform.colorTransform = ct;
			}
		}
		private function selectImage(image:ImagePreview,value:Boolean):void{
			if(image){
				var ct:ColorTransform = new ColorTransform();
				if(value){
					ct.color = 0xf0b310;
				}else{
					ct.color = 0xFFFFFF;
				}
				image.background.transform.colorTransform = ct;
			}
		}
		private function onCloseButtonClickedEventHandler(e:MouseEvent):void{
			view.visible = false;
		}
		private function onAddToStageButtonClickedEventHandler(e:MouseEvent):void{
			if(_currentSelectedImage){
				var addEvent:GameEvents = new GameEvents(GameEvents.ADD_NEW_PRESET,true,true);
				addEvent.result = _currentSelectedImage.imageName;
				dispatchEvent(addEvent);
			}
		}
		private function onNextButtonClickedEventHandler(e:MouseEvent):void{
			_currentPage++;
			if(_currentPage == _pagegroup.numChildren){
				_currentPage = 0;	
			}
			_pagegroup.x = _currentPage * -_pageSize;
		}
		private function onPrevButtonClickedEventHandler(e:MouseEvent):void{
			_currentPage--;
			if(_currentPage < 0){
				_currentPage = _pagegroup.numChildren - 1;	
			}
			_pagegroup.x = _currentPage * -_pageSize;
		}
	}
}