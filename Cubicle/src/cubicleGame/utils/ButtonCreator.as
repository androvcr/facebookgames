package cubicleGame.utils
{
	import fl.controls.ColorPicker;
	import fl.events.ColorPickerEvent;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.text.TextFieldAutoSize;

	public final class ButtonCreator
	{
		
		public static function createLabelButtonFunctions(button:LabelButton,label:String,onClickFunction:Function, xPosition:Number):Number{
			button.x = xPosition;
			button.label_txt.autoSize = TextFieldAutoSize.LEFT;
			button.label_txt.text = label;
			button.mouseChildren = false;
			button.buttonMode = true;
			button.addEventListener(MouseEvent.CLICK,onClickFunction);
			button.addEventListener(MouseEvent.ROLL_OVER,SoundManager.playOverButtonSound);
			return (button.width + xPosition);
		}
		
		public static function createToggleButtonFunctions(button:MovieClip,onClickFunction:Function, onRollOverFunction:Function=null, onRollOutFunction:Function=null):void{
			button.toggleButton = true;
			button.selected = false;
			button.buttonMode = true;
			button.addEventListener(MouseEvent.CLICK, onClickFunction);
			if(onRollOverFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OVER,onRollOverFunction);
			else
				button.addEventListener(MouseEvent.ROLL_OVER,onToggleButtonRollOverEventHandler);
			
			if(onRollOutFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OUT,onRollOutFunction);
			else
				button.addEventListener(MouseEvent.ROLL_OUT,onToggleButtonRollOutEventHandler);
		}
		
		public static function createNormalButtonFunctions(button:MovieClip,onClickFunction:Function, onRollOverFunction:Function=null, onRollOutFunction:Function=null):void{
			button.mouseChildren = false;
			button.buttonMode = true;
			button.addEventListener(MouseEvent.CLICK,onClickFunction);
			if(onRollOverFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OVER,onRollOverFunction);
			if(onRollOutFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OUT,onRollOutFunction);
		}
		
		public static function createColorPickerButtonFunctions(button:ColorPicker,onClickFunction:Function, onRollOverFunction:Function=null, onRollOutFunction:Function=null):void{
			button.addEventListener(ColorPickerEvent.CHANGE,onClickFunction);
			if(onRollOverFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OVER,onRollOverFunction);
			if(onRollOutFunction!=null)
				button.addEventListener(MouseEvent.ROLL_OUT,onRollOutFunction);
		}
		
		public static function setGlowEffect(component:DisplayObject, value:uint=0):void{
			if(value != 0){
				var glow:GlowFilter = new GlowFilter();
				glow.color = value;
				glow.alpha = 1;
				glow.blurX = 10;
				glow.blurY = 10;
				glow.quality = BitmapFilterQuality.MEDIUM;
				component.filters = [glow];
			}else{
				component.filters = null;
			}
		}
		private static function onToggleButtonRollOverEventHandler(e:MouseEvent):void{
			var button:MovieClip = e.currentTarget as MovieClip;
			button.gotoAndStop("_tooltip");
		}
		private static function onToggleButtonRollOutEventHandler(e:MouseEvent):void{
			var button:MovieClip = e.currentTarget as MovieClip;
			button.gotoAndStop("_"+button.selected);
		}
	}
}