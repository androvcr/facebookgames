﻿package cubicleGame.utils {
	/*
	 * The skew class can skew bitmaps to 4 given points, it also provides multiple methods of improving the distortion quality
	 * @license Creative Commons by-nc-sa
	 * @author Maxim Sprey <maximsprey@hotmail.com>
	 * @version 1.0
	 */
	import flash.geom.Matrix;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	public class Skew {
		public var smooth:Boolean;
		public var RP:Boolean;
		private var UVarray:Array;
		private var XYarray:Array;
		public var debug:Boolean;
		
		public function Skew(Smoothing:Boolean,db:Boolean):void {
			smooth = Smoothing;
			debug = db;
			UVarray = [];
			XYarray = [];
		}
		
		public function AASkew(pa:Array,pb:Array,pc:Array,pd:Array,bd:BitmapData,cont:*,AAh:uint,AAv:uint):void {
			// Create an AA*AA grid of squares
			UVMap(bd.width,bd.height,AAh,AAv);
			XYMap(pa,pb,pc,pd,bd.width,bd.height,AAh,AAv);
			for(var i:uint = 0;i<(AAh);i++){
				for(var j:uint = 0;j<(AAv);j++){
					var a:Array = [XYarray[j][i][0],XYarray[j][i][1]];
					var b:Array = [XYarray[j+1][i][0],XYarray[j+1][i][1]];
					var c:Array = [XYarray[j+1][i+1][0],XYarray[j+1][i+1][1]];
					var d:Array = [XYarray[j][i+1][0],XYarray[j][i+1][1]];
					if(debug){
						cont.graphics.lineStyle(2,0xff0000,1);
					}else{
						cont.graphics.lineStyle(0,0,0);
					}
					AAtransformer(a,b,c,d,i,j,bd,cont,AAh,AAv);
				}
			}
		}
		
		private function UVMap(w:Number,h:Number,AAh:uint,AAv:uint):void {
			var hS:Number = w/AAh;
			var vS:Number = h/AAv;
			for(var i:uint = 0;i<=AAh;i++){
				UVarray[i] = [];
				for(var j:uint = 0;j<=AAv;j++){
					UVarray[i][j] = [(i*hS),(j*vS)];
				}
			}
		}
		
		private function XYMap(pa:Array,pb:Array,pc:Array,pd:Array,w:Number,h:Number,AAh:uint,AAv:uint):void {
			var lVec:Array = [pc[0]-pa[0],pc[1]-pa[1]];
			var rVec:Array = [pd[0]-pb[0],pd[1]-pb[1]];
			var p:Array = [];
			var pL:Array = [];
			var pR:Array = [];
			var xc:Number;
			var yc:Number;
			for(var i:uint = 0;i<=AAh;i++){
				XYarray[i] = [];
				for(var j:uint = 0;j<=AAh;j++){
					p = UVarray[i][j];
					XYarray[i][j] = [];
					xc = p[0]/w;
					yc = p[1]/h;
					pL[0] = pa[0]+(yc*lVec[0]);
					pL[1] = pa[1]+(yc*lVec[1]);
					pR[0] = pb[0]+(yc*rVec[0]);
					pR[1] = pb[1]+(yc*rVec[1]);
					XYarray[i][j][0] = pL[0]+(pR[0]-pL[0])*xc;
					XYarray[i][j][1] = pL[1]+(pR[1]-pL[1])*xc;
				}
			}
		}
		
		public function AAtransformer(pa:Array,pb:Array,pc:Array,pd:Array,i:uint,j:uint,bd:BitmapData,cont:*,AAh:uint,AAv:uint):void {
			var bW:uint = bd.width;
			var bH:uint = bd.height;
			var inMat:Matrix = new Matrix();
			var outMat:Matrix = new Matrix();
			var ia:Array = [UVarray[j][i][0],UVarray[j][i][1]];
			var ib:Array = [UVarray[j+1][i][0],UVarray[j+1][i][1]];
			var ic:Array = [UVarray[j+1][i+1][0],UVarray[j+1][i+1][1]];
			var id:Array = [UVarray[j][i+1][0],UVarray[j][i+1][1]];
			inMat.a = (ib[0]-ia[0])/bW;
			inMat.b = (ib[1]-ia[1])/bW;
			inMat.c = (id[0]-ia[0])/bH;
			inMat.d = (id[1]-ia[1])/bH;
			inMat.tx = ia[0];
			inMat.ty = ia[1];
			outMat.a = ((pb[0]-pa[0])/bW);
			outMat.b = (pb[1]-pa[1])/bW;
			outMat.c = (pd[0]-pa[0])/bH;
			outMat.d = ((pd[1]-pa[1])/bH);
			outMat.tx = pa[0];
			outMat.ty = pa[1];
			inMat.invert();
			inMat.concat(outMat);
			cont.graphics.beginBitmapFill(bd,inMat,true,smooth);
			cont.graphics.moveTo(pa[0],pa[1]);
			cont.graphics.lineTo(pb[0],pb[1]);
			cont.graphics.lineTo(pd[0],pd[1]);
			cont.graphics.lineTo(pa[0],pa[1]);
			cont.graphics.endFill();
			inMat.a = (ic[0]-id[0])/bW;
			inMat.b = (ic[1]-id[1])/bW;
			inMat.c = (ic[0]-ib[0])/bH;
			inMat.d = (ic[1]-ib[1])/bH;
			inMat.tx = id[0];
			inMat.ty = id[1];
			outMat.a = ((pc[0]-pd[0])/bW);
			outMat.b = (pc[1]-pd[1])/bW;
			outMat.c = (pc[0]-pb[0])/bH;
			outMat.d = ((pc[1]-pb[1])/bH);
			outMat.tx = pd[0];
			outMat.ty = pd[1];
			inMat.invert();
			inMat.concat(outMat);
			cont.graphics.beginBitmapFill(bd,inMat,true,smooth);
			cont.graphics.moveTo(pc[0],pc[1]);
			cont.graphics.lineTo(pd[0],pd[1]);
			cont.graphics.lineTo(pb[0],pb[1]);
			cont.graphics.lineTo(pc[0],pc[1]);
			cont.graphics.endFill();
		}
		
		public function transformer(pa:Array,pb:Array,pc:Array,pd:Array,bd:BitmapData,cont:*,AAh:uint,AAv:uint):void {
			var bW:uint = bd.width;
			var bH:uint = bd.height;
			var inMat:Matrix = new Matrix();
			var outMat:Matrix = new Matrix();
			outMat.a = ((pb[0]-pa[0])/bW);
			outMat.b = (pb[1]-pa[1])/bW;
			outMat.c = (pd[0]-pa[0])/bH;
			outMat.d = ((pd[1]-pa[1])/bH);
			outMat.tx = pa[0];
			outMat.ty = pa[1];
			cont.graphics.beginBitmapFill(bd,outMat,true,smooth);
			cont.graphics.moveTo(pa[0],pa[1]);
			cont.graphics.lineTo(pb[0],pb[1]);
			cont.graphics.lineTo(pd[0],pd[1]);
			cont.graphics.lineTo(pa[0],pa[1]);
			cont.graphics.endFill();
			outMat.a = ((pc[0]-pd[0])/bW);
			outMat.b = (pc[1]-pd[1])/bW;
			outMat.c = (pc[0]-pb[0])/bH;
			outMat.d = ((pc[1]-pb[1])/bH);
			outMat.tx = pd[0];
			outMat.ty = pd[1];
			cont.graphics.beginBitmapFill(bd,outMat,true,smooth);
			cont.graphics.moveTo(pc[0],pc[1]);
			cont.graphics.lineTo(pd[0],pd[1]);
			cont.graphics.lineTo(pb[0],pb[1]);
			cont.graphics.lineTo(pc[0],pc[1]);
			cont.graphics.endFill();
		}
	}
}