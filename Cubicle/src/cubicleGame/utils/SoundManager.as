package cubicleGame.utils
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;

	public final class SoundManager{
		
		//private static var _clickButtonSound:ClickButtonSound = new ClickButtonSound();
		//private static var _overButtonSound:OverButtonSound = new OverButtonSound();
		
		private static var _clickChannel:SoundChannel = new SoundChannel();
		private static var _introChannel:SoundChannel = new SoundChannel();
		private static var _overChannel:SoundChannel = new SoundChannel();
		private static var _enabled:Boolean = false;
		
		public static function playClickButtonSound(e:MouseEvent=null):void{
			if(SoundManager._enabled){
				_overChannel.stop();
				_clickChannel.stop();
				//_clickChannel = _clickButtonSound.play();
			}
		}
		
		public static function playOverButtonSound(e:MouseEvent=null):void{
			if(SoundManager._enabled){
				_overChannel.stop();
				//_overChannel = _overButtonSound.play();
			}
		}
		
		public static function addButtonSounds(display:MovieClip):void{
			if(SoundManager._enabled){
				display.addEventListener(MouseEvent.CLICK,playClickButtonSound);
				display.addEventListener(MouseEvent.ROLL_OVER,playOverButtonSound);
			}
		}
	}
}