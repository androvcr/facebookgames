﻿package 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.MouseEvent;
	import flash.display.StageDisplayState;

	public class Preloader extends MovieClip
	{
		var contentLoader:Loader;
		public var startButton:MovieClip;
		public var mailButton:MovieClip;

		public function Preloader(){
			loadContent("CubicleGame.swf");
			startButton.visible = false;
			startButton.buttonMode = true;
			startButton.addEventListener(MouseEvent.CLICK, onStartButtonClickedEventHandler);
		}

		private function loadContent(url:String):void
		{
			contentLoader = new Loader();
			contentLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loading);
			contentLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, contentLoaded);
			contentLoader.load(new URLRequest(url));
		}
		private function contentLoaded(evt:Event):void
		{
			startButton.visible = true;
			gotoAndPlay(2);
		}
		private function loading(evt:ProgressEvent):void
		{
			var loaded:Number = evt.bytesLoaded / evt.bytesTotal;
			setBarProgress(loaded);
		}
		private function setBarProgress(value:Number)
		{
			progressbar.bar.scaleX = value;
		}
		private function onStartButtonClickedEventHandler(e:MouseEvent):void{
			addChild(contentLoader);
		}
	}

}