<?php	 	 	 	 	 	 	 	 	 	 
	error_reporting(E_ALL);
	ini_set('display_errors','On');
	
	require_once("model/FacebookConnect.php");
	require_once("model/FlashPage.php");
	
	$facebook = new FacebookConnect();
	$userphoto = $facebook->getCurrentUserPhoto(FacebookConnect::PHOTO_SQUARE);
	$userid = $facebook->getCurrentUser();
	
	if(!$userid){
		//$facebook->redirect();
	}else{
		$info = $facebook->getUserFullName($userid);
		header("Content-type: text/xml");
		?>
		<data>
			<id><?php	 	 	 	 	 	 	 	 	 	  echo $userid; ?></id>
			<photo><?php	 	 	 	 	 	 	 	 	 	  echo $userphoto; ?></photo>
			<name><?php	 	 	 	 	 	 	 	 	 	  echo $info; ?></name>
		</data>
		<?php	 	 	 	 	 	 	 	 	 	 
	}
?>