<?php
	error_reporting(E_ALL);
	ini_set('display_errors','On');
	
	require_once("model/FacebookConnect.php");
	require_once("model/FlashPage.php");
	
	$facebook = new FacebookConnect();
	$userid = $facebook->getCurrentUser();
	
	if(!$userid){
		$facebook->redirect();
	}else{
		if(isset($_POST['imageData'])){
			try{
				$imageData = $_POST['imageData'];
				$decodedImage = base64_decode($imageData);
				$today = date("YmdHis");
				$fileName = $userid.'_'.$today.'.jpg';
				@chdir("FotosEnviadas/");
				file_put_contents($fileName,$decodedImage);
				$finalURL = '@'. realpath("$fileName");
				if($facebook->changeAccount()){
					$data = $facebook->uploadPhoto($finalURL);
					if($data != null && isset($data['id'])){
						echo $data['id'];
					}else{
						echo "CantUploadToFacebook";	
					}
				}else{
					echo "CantChangesAccount";
				}
			}catch(ErrorException $e){
			}
		}else{
			$facebook->redirect();
		}
	}
?>