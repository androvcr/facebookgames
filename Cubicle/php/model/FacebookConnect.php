<?php
	error_reporting(E_ALL);
	ini_set('display_errors','On');
	
	require_once ('inc/facebook.php');
	require_once ('MainData.php');
	
	class FacebookConnect{
		 private $facebook = null;
		 private $current_user = null;
		 private $signed_request = null;
		 private $fanpage_token = null;
		 
		 const PHOTO_SMALL = 'small';
		 const PHOTO_SQUARE = 'square';
		 const PHOTO_NORMAL = 'normal';
		 const PHOTO_LARGE = 'large';
		
		public function __construct(){
			try{
				$this->facebook = new Facebook(array('appId'  => MainData::APP_ID,'secret' => MainData::SECRET_KEY,'cookie' => true, 'fileUpload' => true));
				//$this->facebook->setFileUploadSupport(true);
				$this->signed_request = $this->facebook->getSignedRequest();
				$this->current_user = $this->facebook->getUser();
			}catch(FacebookApiException $e) {
				return $e;
			}
		}
		
		public function getCurrentUser(){
			return $this->current_user;
		}
		
		public function getCurrentUserInfo(){
			$user = null;
			if($this->current_user){
				try {
					$facebook_query = 'SELECT uid, name, username from user where uid = ' . $this->current_user;
					$user = $this->facebook->api(array('method' => 'fql.query','query' => $facebook_query,));
					return $user;
				}catch(FacebookApiException $e) {
  					return null;
  				}
			}	
			return null;
		}
		public function redirect(){
			$params = array('redirect_uri' => MainData::REDIRECT_URL,
							'scope' => 'publish_stream,offline_access,read_stream,manage_pages,photo_upload,publish_actions');
			$auth_url = $this->facebook->getLoginUrl($params);
			echo("<script> top.location.href='" . $auth_url . "'</script>");
		}
		
		public function getCurrentUserPhoto($photoSize){
			$photo = null;
			if($this->current_user){
				try {
					$photo = "https://graph.facebook.com/$this->current_user/picture?type=$photoSize";
					return $photo;
				}catch(FacebookApiException $e) {
  					return null;
  				}
			}	
			return null;
		}
		
		public function getUserPhoto($userid, $photoSize){
			$photo = null;
			if($this->current_user){
				try {
					$facebook_query = 'SELECT '.$photoSize.' from user where uid = ' . $userid;
					$photo = $this->facebook->api(array('method' => 'fql.query','query' => $facebook_query,));
					$photo = $photo[0][$photoSize];
					return $photo;
				}catch(FacebookApiException $e) {
  					return null;
  				}
			}	
			return null;
		}
		
		public function getUserFullName($userid){
			$photo = null;
			if($this->current_user){
				try {
					$facebook_query = 'SELECT name from user where uid = ' . $userid;
					$data = $this->facebook->api(array('method' => 'fql.query','query' => $facebook_query,));
					$data = $data[0]['name'];
					return $data;
				}catch(FacebookApiException $e) {
  					return null;
  				}
			}	
			return null;
		}
		
		public function uploadPhoto($finalURL){
			$userid = $this->current_user;
			$userData = $this->getCurrentUserInfo();
			$fullname = $userData[0]['name'];
			$nickname = $userData[0]['username'];
			$message = $fullname. " Uploaded This Photo \n http://www.facebook.com/$nickname";
			try{
				$attachment = array(
					'message' => $message,
					'image' => $finalURL,
					'aid' => MainData::ALBUM_ID,
					'no_story' => 0,
					'access_token' => $this->fanpage_token
				);
				$data = $this->facebook->api(MainData::ALBUM_ID . '/photos/', 'post', $attachment);
				$photo_id = $data['id'];
				$tagMe = array(
			        'tag_uid' => $userid,
			        'tag_text' => 'My Bosz Cubicle!!',
			        'x' => 50,
			        'y' => 30
			    );
				$tagMe = json_encode($tagMe);
				$tagMe = array($tagMe);
				$this->facebook->api('/' . $photo_id . '/tags', 'post', array('tags' => $tagMe));
			}catch(FacebookApiException $e) {
				return $data;
			}
			return $data;
		}

		public function changeAccount(){
			try{
				//$userid = $this->current_user;
				$userid = '100001217631248';
				$app_id = MainData::APP_ID;
			    $app_secret = MainData::SECRET_KEY;
				
				$params = array('access_token' => MainData::USER_ACCESS_TOKEN);
				$accounts = $this->facebook->api("/$userid/accounts", 'GET', $params);
				foreach($accounts['data'] as $account) {
	 				if( $account['id'] == MainData::FAN_PAGE_ID || $account['name'] == MainData::FAN_PAGE_ID ){
	  					$this->fanpage_token = $account['access_token'];
						return true;
	 				}
				}
				
				return false;	
			}catch(FacebookApiException $e) {
				echo "$e";
				return false;
			}
		}
		
		public function getLikes($userid){
			$facebook_query = "SELECT page_id FROM page_fan WHERE uid=$userid and page_id=496876973656332";
			$data = $this->facebook->api(array('method' => 'fql.query','query' => $facebook_query,));
			return $data;
		}
	}
?>