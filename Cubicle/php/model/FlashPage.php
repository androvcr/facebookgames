<?php
	require_once("MainData.php"); 

	class FlashPage{
		public static function getPage(){
			?>
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<link href="css/style.css" rel="stylesheet" type="text/css" />
				<link href="css/estilo.css" rel="stylesheet" type="text/css" />
				<title>APPLICATION</title>
				<meta name="google" value="notranslate" />         
			    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		        <!-- Include CSS to eliminate any default margins/padding and set the height of the html element and 
		             the body element to 100%, because Firefox, or any Gecko based browser, interprets percentage as 
		             the percentage of the height of its parent container, which has to be set explicitly.  Fix for
		             Firefox 3.6 focus border issues.  Initially, don't display flashContent div so it won't show 
		             if JavaScript disabled.
		        -->
		        <!-- Enable Browser History by replacing useBrowserHistory tokens with two hyphens -->
		        <!-- BEGIN Browser History required section -->
		        <link rel="stylesheet" type="text/css" href="history/history.css" />
		        <script type="text/javascript" src="history/history.js"></script>
		        <!-- END Browser History required section -->  
			            
		        <script type="text/javascript" src="swfobject.js"></script>
		        <script type="text/javascript">
		            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
		            var swfVersionStr = "10.2.0";
		            // To use express install, set to playerProductInstall.swf, otherwise the empty string. 
		            var xiSwfUrlStr = "playerProductInstall.swf";
		            var flashvars = {
		            };
		            var params = {};
		            params.quality = "high";
		            params.bgcolor = "#FFFFFF";
		            params.allowscriptaccess = "always";
		            params.allowfullscreen = "true";
		            params.wmode = "transparent";
		            var attributes = {};
		            attributes.id = "APPLICATION";
		            attributes.name = "APPLICATION";
		            attributes.align = "middle";
		            swfobject.embedSWF(
		                "<?php echo MainData::APP_NAME; ?>", "flashContent", 
		                "<?php echo MainData::APP_WIDTH; ?>", "<?php echo MainData::APP_HEIGHT; ?>", 
		                swfVersionStr, xiSwfUrlStr, 
		                flashvars, params, attributes);
		            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
		            swfobject.createCSS("#flashContent", "display:block;text-align:left;");
		        </script>
		        <script>
				  window.fbAsyncInit = function() {
				    FB.init({
					    appId : '<?php echo MainData::APP_ID; ?>',
						status : true, // check login status
					    cookie : true, // enable cookies to allow the server to access the session
					    xfbml  : true  // parse XFBML
					});
				
				    FB.Canvas.setAutoGrow();
				  };
				
				  // Load the SDK Asynchronously
				  (function(d){
				     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
				     js = d.createElement('script'); js.id = id; js.async = true;
				     js.src = "//connect.facebook.net/en_US/all.js";
				     d.getElementsByTagName('head')[0].appendChild(js);
				   }(document));
				   
				  
				  function inviteFriendsToCubicle(){
				  	FB.ui({
						method: 'apprequests',
						message: 'Invite all your friends to play BoszCubicle and have some fun!'
					});
				  }
				  
				  function showMessage(text){
				  	//alert('Mensaje: ' + text);
				  }
				</script>
			</head>
			<body>
				<!-- SWFObject's dynamic embed method replaces this alternative HTML content with Flash content when enough 
			             JavaScript and Flash plug-in support is available. The div is initially hidden so that it doesn't show
		             when JavaScript is disabled.
		        -->
		        <div id="flashContent">
		            <p>
		                To view this page ensure that Adobe Flash Player version 
		                10.2.0 or greater is installed. 
		            </p>
		            <script type="text/javascript"> 
		                var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
		                document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
		                                + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
		            </script> 
		        </div>
			        
		        <noscript>
		            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<?php echo MainData::APP_WIDTH; ?>" height="<?php echo MainData::APP_HEIGHT; ?>" id="APPLICATION">
		                <param name="movie" value="<?php echo MainData::APP_NAME; ?>" />
		                <param name="quality" value="high" />
		                <param name="bgcolor" value="#FFFFFF" />
		                <param name="allowScriptAccess" value="always" />
		                <param name="wmode" value="transparent"/>
		                <param name="allowFullScreen" value="true" />
		                <param name="FlashVars" value="" />
		                <!--[if !IE]>-->
		                <object type="application/x-shockwave-flash" data="<?php echo MainData::APP_NAME; ?>" width="<?php echo MainData::APP_WIDTH; ?>" height="<?php echo MainData::APP_HEIGHT; ?>">
		                    <param name="quality" value="high" />
		                    <param name="bgcolor" value="#FFFFFF" />
		                    <param name="allowScriptAccess" value="always" />
		                    <param name="allowFullScreen" value="true" />
		                    <param name="FlashVars" value="" />
		                <!--<![endif]-->
		                <!--[if gte IE 6]>-->
		                    <p> 
		                        Either scripts and active content are not permitted to run or Adobe Flash Player version
		                        10.2.0 or greater is not installed.
		                    </p>
		                <!--<![endif]-->
		                    <a href="http://www.adobe.com/go/getflashplayer">
		                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
		                    </a>
		                <!--[if !IE]>-->
		                </object>
		                <!--<![endif]-->
		            </object>
		        </noscript>
			</body>
			</html>
			<?php
		}
	}
?>